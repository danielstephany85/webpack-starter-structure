const HtmlWebpackPlugin = require('html-webpack-plugin'); //installed via npm
const webpack = require('webpack'); //to access built-in plugins
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const path = require('path');
var isprod = process.env.NODE_ENV === 'production'; //checking the value of NODE-ENV which is set in package.json
var cssDev = ['style-loader', 'css-loader','sass-loader']; //setting the loaders to be used if isprod is false
//setting the loaders to be used if isprod is false
var cssProd = ExtractTextPlugin.extract({   
    fallback: "style-loader",
    use: ['css-loader','sass-loader'],
    publicPath: '/dist'
});
//selecting the loaders based on env var
var cssConfig = isprod ? cssProd : cssDev;



module.exports = {
  entry: './src/assets/js/main/main.js',
  output: {
    filename: 'app.bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: cssConfig
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        options: { presets: ['es2015'] }  
      },
      {
        test: /\.(jpg|png|svg|gif)$/,
        use: ["file-loader?name=[name].[ext]&outputPath=assets/imgs/", //loades imgs into dist/assets/imgs
            "image-webpack-loader" //optimizes imgs
        ]
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin({
        filename: "/assets/style.css",
        disable: !isprod
    }),
    new HtmlWebpackPlugin({
        filename: 'index.html',
        template: './src/views/index.html',
        hash: true
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
  ],
  devServer: {
    contentBase: path.join(__dirname, "dist"),
    compress: true,
    port: 3000,
    stats: "errors-only",
    open: true
    }
};